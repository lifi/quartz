---
lang: de
---

# Quartz

[![Aktuelle Version](https://gitlab.gwdg.de/lifi/quartz/-/badges/release.svg?key_text=Aktuelle+Version)](https://lifi.pages.gwdg.de/quartz/install/linux/)
[![Dokumentation](https://gitlab.gwdg.de/lifi/quartz/-/badges/release.svg?key_text=Dokumentation)](https://lifi.pages.gwdg.de/quartz/)

QEMU RISC-V 64bit VM mit einem Mini-Linux für Technische Informatik.

![Quartz Demo](./docs/img/quartzdemo.gif)

## Dokumentation

Alle weiteren Informationen gibt es in der Dokumentation:

https://lifi.pages.gwdg.de/quartz/


