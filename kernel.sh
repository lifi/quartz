#!/bin/bash

# https://risc-v-getting-started-guide.readthedocs.io/en/latest/linux-qemu.html

set -e

# Install requirements
# sudo apt install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev ninja-build git cmake libglib2.0-dev python3 python3-pip 

export KERNELVERSION="6.1.65"
export KERNELURL="https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-${KERNELVERSION}.tar.xz"
export KERNELSRCDIR="linux-${KERNELVERSION}"

export MAKEARGS="VERBOSE=1 KBUILD_VERBOSE=0 CFLAGS=-fcommon CPPFLAGS=-fcommon ARCH=riscv CROSS_COMPILE=riscv64-linux-gnu- -j $(nproc)"

if [ "$1" = "env" ]; then
	return 0
elif [ "$1" = "clean" ]; then
	rm -rf "$(dirname "$0")/riscv64-linux"
	exit 0
fi

pushd "$(dirname "$0")" > /dev/null

mkdir -p "riscv64-linux"
pushd "riscv64-linux" > /dev/null

# Download kernel source code if missing
if [ ! -d "${KERNELSRCDIR}" ]; then
	#git clone https://github.com/torvalds/linux linux
	#git checkout v5.4.0
	#git checkout v5.4
	#git checkout v6.2

	wget "${KERNELURL}" -O linux.tar.xz
	# TODO verify signature
	tar xf linux.tar.xz
	rm -f linux.tar.xz
	rm -vf "${KERNELSRCDIR}/.config"
	ln -sf "${KERNELSRCDIR}" "./linux"
fi
if [ ! -f "${KERNELSRCDIR}/.config" ]; then
	cp -v "../kernel.config" "${KERNELSRCDIR}/.config"
fi

if [ "$1" = "menuconfig" ]; then
	cp -v "../kernel.config" "../kernel.config.old"

	pushd "${KERNELSRCDIR}" >/dev/null

	cp -v "../../kernel.config" ".config"

	#make ${MAKEARGS} help
	#exit 0

	#make ${MAKEARGS} clean
	#make ${MAKEARGS} mrproper

	# Create the config
	#make ${MAKEARGS} defconfig
	#make ${MAKEARGS} olddefconfig
	#make ${MAKEARGS} oldconfig
	#make ${MAKEARGS} tinyconfig

	# Edit the config
	make ${MAKEARGS} menuconfig

	cp -v ".config" "../../kernel.config"

	echo "====== DIFF ====="
	./scripts/diffconfig ../../kernel.config.old ../../kernel.config
	echo "====== /DIFF ====="

	popd >/dev/null
elif [ "$1" = "build" ]; then
	set -x

	# sudo apt install qemu-system-misc opensbi
	# replaces:
	#   git clone https://github.com/qemu/qemu
	#   pushd qemu
	#   git checkout v5.0.0
	#   ./configure --target-list=riscv64-softmmu
	#   make -j $(nproc)
	#   sudo make install
	#   popd

	# sudo apt install gcc-riscv64-linux-gnu

	riscv64-linux-gnu-gcc --version

	pushd "${KERNELSRCDIR}"

	#make ${MAKEARGS} clean
	#make ${MAKEARGS} -j $(nproc) mrproper

	# Compile the kernel
	make ${MAKEARGS}

	# Build kernel package
	make ${MAKEARGS} dir-pkg LOCALVERSION=-quartz

	popd

elif [ "$1" = "install" ] && [ "$#" = "2" ]; then
	#export INSTALLPATH="../../../../cptooptquartz/boot"
	export INSTALLPATH="${2}"
	#rm -rf "${INSTALLPATH}"
	ls -lah "${INSTALLPATH}"
	mkdir -p "${INSTALLPATH}"

	pushd "${KERNELSRCDIR}"
	# Install kernel
	make ${MAKEARGS} INSTALL_PATH=${INSTALLPATH} install
	popd

	pushd "${INSTALLPATH}"
	ln -svf config-6.1.65-quartz config-quartz
	ln -svf System.map-6.1.65-quartz System.map-quartz
	ln -svf vmlinuz-6.1.65-quartz vmlinuz-quartz
	popd
else
	cat <<EOF >&2
Usage:
$0 clean
	- Delete the riscv64-linux dir and thus the build.
$0 build
	- Build the kernel.
$0 menuconfig
	- Edit kernel.config.
$0 install <INSTALLPATH>
	- Install the kernel.
	- INSTALLPATH: absolute path to boot dir
. $0 env
	- Get environment variables.
EOF
	exit 1
fi
popd > /dev/null
popd > /dev/null
