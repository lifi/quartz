# Maintaining Quartz

QEMU RISC-V 64bit VM mit einem Mini-Linux für Technische Informatik.

## Projektanforderung


- Ein Image eines Mini-Linux für QEMU (qemu-system-riscv32), dass in der Vorlesung und Übung von Technische Informatik eingesetzt werden kann.
- Schön wäre ein Integration in Lifi, die Möglichkeit das Image im Pool zu starten und u.U. auch auf dem eigenen Rechner.
- Keine Grafik notwendig.
- Soll GNU Toolchain beinhalten.
- Fertige VM muss nicht klein sein.
- Sinnvoll etwas weitgehend fertiges zu nehmen, z.B. meta-riscv <https://github.com/riscv/meta-riscv>.

Alter Ansatz:
> Bis jetzt habe ich eine online Version verwendet, die auf TinyEMU aufbaut und Buildroot verwendet <https://bellard.org/tinyemu/buildroot.html>. Buildroot hat aber eigentlich eine Cross-Compiler-Ansatz und die GNU Toolchain in das Image zu integrieren ist Fummelarbeit <https://luplab.cs.ucdavis.edu/2022/01/06/buildroot-and-compiler-on-target.html>.

## Quartz installieren

```bash
sudo apt install qemu-system-misc screen openssh-client curl bash grep sed gzip coreutils
sudo pacman -S qemu-system-riscv screen openssh curl bash grep sed gzip coreutils

# Install version 1.0.0 system wide
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | sudo RELEASEVERSION="1.0.0" PREFIX="/usr/local" bash
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | sudo RELEASEVERSION="1.0.0" PREFIX="/usr" bash

# Install version 1.0.0 in users HOME directory
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | RELEASEVERSION="1.0.0" PREFIX="$HOME/.local" bash

# Just print this help message
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | PRINTHELP=yes bash

# Install version 1.0.0 system wide but don't install desktop entry
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | sudo RELEASEVERSION="1.0.0" PREFIX="/usr/local" INSTALLDESKTOPENTRY=no bash

# Install version 1.0.0 in users HOME directory but don't check dependencies
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | RELEASEVERSION="1.0.0" PREFIX="$HOME/.local" CHECKDEPENDENCIES=no bash
```


## Quartz bauen

```bash
sudo apt install mmdebstrap qemu-user-static binfmt-support libguestfs-tools u-boot-tools u-boot-qemu
sudo apt install autoconf automake autotools-dev curl libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev ninja-build git cmake libglib2.0-dev python3 python3-pip gzip
sudo apt install gcc-riscv64-linux-gnu
sudo apt install jq coreutils grep desktop-file-utils
make clean-all
make kernel
make build
```

### gebautes Quartz ausführen

```bash
./test.sh
```


## Quartz releasen

https://gitlab.com/gitlab-org/cli/#installation
```bash
desktop-file-validate Quartz.desktop
./release.sh <RELEASEVERSION>
```



## Links
- https://wiki.debian.org/RISC-V
- https://risc-v-getting-started-guide.readthedocs.io/en/latest/linux-qemu.html
- https://github.com/yuzibo/riscv32
- https://wiki.debian.org/RISC-V/32
- https://riscv.org/technical/specifications/
- https://people.debian.org/~gio/dqib/
- https://colatkinson.site/linux/riscv/2021/01/27/riscv-qemu/
- https://wiki.ubuntu.com/ReducingDiskFootprint#Drop_unnecessary_packages
- https://colatkinson.site/linux/riscv/2021/01/27/riscv-qemu/
- https://www.qemu.org/docs/master/system/target-riscv.html
- https://www.qemu.org/docs/master/system/riscv/virt.html
### Kernel
- https://kernel.org/
- https://www.maketecheasier.com/build-custom-kernel-ubuntu/
- https://www.debian.org/doc/manuals/debian-kernel-handbook/
- https://www.debian.org/doc/manuals/debian-kernel-handbook/ch-packaging.html
- https://ibug.io/blog/2019/04/os-lab-1/
- https://firasuke.github.io/DOTSLASHLINUX/post/booting-the-linux-kernel-without-an-initrd-initramfs/
- https://www.debian.org/doc//manuals/debian-handbook/sect.kernel-compilation.pl.html
- https://risc-v-machines.readthedocs.io/en/latest/linux/simple/
- https://www.kernel.org/doc/html/latest/kbuild/makefiles.html
- https://github.com/bsbernd/tiny-qemu-virtio-kernel-config
- https://wiki.alpinelinux.org/wiki/Enable_Serial_Console_on_Boot#qemu
- https://unix.stackexchange.com/questions/161674/are-there-alternatives-to-using-udev
### Networking
- https://insujang.github.io/2021-03-10/virtio-and-vhost-architecture-part-1/
- https://insujang.github.io/2021-03-15/virtio-and-vhost-architecture-part-2/
- https://www.redhat.com/en/blog/introduction-virtio-networking-and-vhost-net
- http://blog.vmsplice.net/2011/09/qemu-internals-vhost-architecture.html
- https://programmersought.com/article/38004209835/
- https://programmersought.com/article/38004209835/#VirtIO__113
- https://www.redhat.com/en/blog/virtio-networking-first-series-finale-and-plans-2020
- https://www.redhat.com/en/blog/virtio-devices-and-drivers-overview-headjack-and-phone
- https://www.redhat.com/en/blog/learn-about-virtio-networking
- https://wiki.qemu.org/Documentation/Networking
### Desktop Entries
- https://wiki.archlinux.org/title/Desktop_entries
- https://freedesktop.org/wiki/Howto_desktop_files/
- https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s06.html
- https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
- https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s07.html
- https://specifications.freedesktop.org/desktop-entry-spec/latest/ar01s11.html
- https://specifications.freedesktop.org/menu-spec/menu-spec-1.0.html
- https://www.freedesktop.org/wiki/Software/desktop-file-utils/


