#!/bin/bash
#################################################
# easyinstall.sh                                #
# Quartz: https://gitlab.gwdg.de/lifi/quartz    #
#################################################

EASYINSTALLURL="https://lifi.pages.gwdg.de/quartz/easyinstall.sh"

set -Eeo pipefail # Exit on error
trap cleanup SIGINT SIGTERM ERR EXIT

ISSUCCESS=false


##################### FUNCTIONS #####################

msg() {
	#echo -e "\e[1;33m[$(date +%H:%M:%S)] \e[0m${*}"
	echo -e "\e[0m${*}"
}

info() {
	msg "\e[36mINFO: \e[0m${*}"
}

task() {
	msg "\e[33mTASK: \e[0m${*}"
}

warn() {
	msg "\e[33mWARNING: \e[0m${*}" >&2
}

die() {
	echo -ne "\n"
	msg "\e[31mFATAL ERROR: \e[0m${*}" >&2
	echo -ne "\n"
	exit 1
}

cleanup() {
	trap - SIGINT SIGTERM ERR EXIT

	if [ "$ISSUCCESS" = "true" ]; then
		exit 0
	fi

	printf '\e[?25h\n\r\e[2K\e[0m' >&2
	cat - <<EOF >&2
   +----------------------------------------------------------------------+
   |       It seems an error occurred while running easyinstall.sh.       |
   |     Please write a bug report on the issue tracker if necessary:     |
   |             https://gitlab.gwdg.de/lifi/quartz/-/issues              |
   +----------------------------------------------------------------------+
EOF
}


check_dependency() {
	if ! command -v "${1}" > /dev/null ; then
		die "Command \"${1}\" not found. Please install ${2}."
	fi
}

check_dependencies() {
	check_dependency cat "the GNU Coreutils"
	check_dependency chmod "the GNU Coreutils"
	check_dependency date "the GNU Coreutils"
	check_dependency mkdir "the GNU Coreutils"
	check_dependency rm "the GNU Coreutils"
	check_dependency sleep "the GNU Coreutils"
	check_dependency touch "the GNU Coreutils"
	check_dependency env "the GNU Coreutils"
	check_dependency seq "the GNU Coreutils"
	check_dependency tail "the GNU Coreutils"
	check_dependency test "the GNU Coreutils"
	check_dependency cat "the GNU Coreutils"
	check_dependency sha256sum "the GNU Coreutils"
	#check_dependency realpath "the GNU Coreutils"
	#check_dependency dirname "the GNU Coreutils"
	check_dependency install "the GNU Coreutils" # Only required for easyinstall.sh
	check_dependency mktemp "the GNU Coreutils" # Only required for easyinstall.sh
	#check_dependency tr "the GNU Coreutils"
	#check_dependency id "the GNU Coreutils"
	#check_dependency stat "the GNU Coreutils"
	check_dependency bash "the GNU Bourne Again SHell"
	check_dependency grep "GNU grep"
	check_dependency sed "GNU sed"
	check_dependency gzip "GNU compression utilities"
	check_dependency screen "GNU screen" #screen: /usr/bin/screen
	check_dependency qemu-system-riscv64 "the QEMU emulator (on Debian/Ubuntu: qemu-system-misc)" # qemu-system-misc: /usr/bin/qemu-system-riscv64
	check_dependency ssh "the OpenSSH Client (on Debian/Ubuntu: openssh-client)" #openssh-client: /usr/bin/ssh
	check_dependency ssh-keygen "the OpenSSH Client (on Debian/Ubuntu: openssh-client)"
	#check_dependency ssh-add "the OpenSSH Client"
	#check_dependency ssh-agent "the OpenSSH Client"
	check_dependency curl "Curl"
	#check_dependency git "Git"
}

print_help() {
	cat - <<EOF
Quartz easyinstall.sh help
==========================

This script uses the following environment variables to determine how and where
to install quartz:

- PREFIX is used to determine where quartz should be installed.
  If you are installing quartz on a system level you should use one of the
  following values:
    PREFIX="/usr/local"
    PREFIX="/usr"
  If you are installing quartz in your HOME directory you should use the
  following value:
    PREFIX="\$HOME/.local"
  If PREFIX is not set nothing will be installed and this help message is printed.
  Note: '~' does NOT work. Use '\$HOME'.
  Note: Relative paths are also NOT supported. Use absolute paths.

- RELEASEVERSION is used to determine which version of quartz should be installed
  RELEASEVERSION is given in the format "X.Y.Z". For example:
    RELEASEVERSION="1.0.0"
  You can find the available releases here:
    https://gitlab.gwdg.de/lifi/quartz/-/releases
  If RELEASEVERSION is not set or wrongly formatted nothing will be installed
  and this help message is printed.

- INSTALLDESKTOPENTRY is used to dermine whether the .desktop file and logo
  should be installed. Possible values are:
    INSTALLDESKTOPENTRY=yes
    INSTALLDESKTOPENTRY=no
  Default is yes.

- VALIDATEDOWNLOADS is used to dermine whether this script should check if all
  downloaded files match their expected sha256sums.
  Possible values are:
    VALIDATEDOWNLOADS=yes
    VALIDATEDOWNLOADS=no
  Default is yes.

- CHECKDEPENDENCIES is used to dermine whether this script should check if all
  dependencies are installed.
  Possible values are:
    CHECKDEPENDENCIES=yes
    CHECKDEPENDENCIES=no
  Default is yes.

- PRINTHELP is used to dermine whether this script should not install anything
  and instead just print this help message.
  Possible values are:
    PRINTHELP=yes
    PRINTHELP=no
  Default is no.


Examples
========

# Install version 1.0.0 system wide
curl ${EASYINSTALLURL} | sudo RELEASEVERSION="1.0.0" PREFIX="/usr/local" bash
curl ${EASYINSTALLURL} | sudo RELEASEVERSION="1.0.0" PREFIX="/usr" bash

# Install version 1.0.0 in users HOME directory
curl ${EASYINSTALLURL} | RELEASEVERSION="1.0.0" PREFIX="\$HOME/.local" bash

# Just print this help message
curl ${EASYINSTALLURL} | PRINTHELP=yes bash

# Install version 1.0.0 system wide but don't install desktop entry
curl ${EASYINSTALLURL} | sudo RELEASEVERSION="1.0.0" PREFIX="/usr/local" INSTALLDESKTOPENTRY=no bash

# Install version 1.0.0 in users HOME directory but don't check dependencies
curl ${EASYINSTALLURL} | RELEASEVERSION="1.0.0" PREFIX="\$HOME/.local" CHECKDEPENDENCIES=no bash

EOF
}

# This function is analog to upload_file in release.sh
# Though it will automatically uncompress .gz files.
function download_file() {
	FILENAME="${1}"
	PACKAGENAME="quartz"
	GITLABPROJECTID="33775" # https://gitlab.gwdg.de/lifi/quartz


	DLLINK="https://gitlab.gwdg.de/api/v4/projects/${GITLABPROJECTID}/packages/generic/${PACKAGENAME}/${RELEASEVERSION}/${FILENAME}"
	#echo "Downloading '${SRCFILE}' from ${DLLINK}"
	info "Downloading ${FILENAME}:"
	curl --progress-bar -o "${FILENAME}" "${DLLINK}"

	#glab alias set upload-package 'api projects/:id/packages/generic/$1 -X PUT --input $2'
	#glab upload-package "quartz/${RELEASEVERSION}/${FILENAME}" "${SRCFILE}"
	#glab api "projects/${GITLABPROJECTID}/packages/generic/${PACKAGENAME}/${RELEASEVERSION}/${FILENAME}" -X "PUT" --input "${SRCFILE}"

	# Automatically uncompress downloaded .gz files
	if [[ "${FILENAME}" == *.gz ]]; then
		gzip --force --uncompress "${FILENAME}" || true
	fi
}

# easyinstall.sh must not read from stdin!
#ask_yon() {
#	while true; do
#		printf "(Y/N): "
#		read -r YON
#		YON="$(echo "${YON}" | tr '[:upper:]' '[:lower:]')"
#		if [ "x${YON}" = "xy" ] || [ "x${YON}" = "xyes" ]; then
#			true; return
#		elif [ "x${YON}" = "xn" ] || [ "x${YON}" = "xno" ]; then
#			false; return
#		fi
#	done
#}

##################### MAIN #####################

main() {

	PRINTHELP="${PRINTHELP:-no}"
	INSTALLDESKTOPENTRY="${INSTALLDESKTOPENTRY:-yes}"
	CHECKDEPENDENCIES="${CHECKDEPENDENCIES:-yes}"
	VALIDATEDOWNLOADS="${VALIDATEDOWNLOADS:-yes}"

	if [ "${PRINTHELP}" = "yes" ]; then
		print_help
		ISSUCCESS=true
		exit 0
	elif (! echo "${RELEASEVERSION}" | grep -q -E "^[0-9]+\.[0-9]+\.[0-9]+$") ; then
		print_help
		die "Invalid value for RELEASEVERSION: ${RELEASEVERSION}"
	elif [ -z "${PREFIX}" ]; then
		print_help
		die "PREFIX missing."
	elif [ "${INSTALLDESKTOPENTRY}" != "yes" ] && [ "${INSTALLDESKTOPENTRY}" != "no" ]; then
		print_help
		die "Invalid value for INSTALLDESKTOPENTRY: ${INSTALLDESKTOPENTRY}"
	elif [ "${CHECKDEPENDENCIES}" != "yes" ] && [ "${CHECKDEPENDENCIES}" != "no" ]; then
		print_help
		die "Invalid value for CHECKDEPENDENCIES: ${CHECKDEPENDENCIES}"
	elif [ "${VALIDATEDOWNLOADS}" != "yes" ] && [ "${VALIDATEDOWNLOADS}" != "no" ]; then
		print_help
		die "Invalid value for VALIDATEDOWNLOADS: ${VALIDATEDOWNLOADS}"
	fi

	info "Installing Quartz using the following settings"
	cat - <<EOF
   PREFIX: ${PREFIX}
   RELEASEVERSION: ${RELEASEVERSION}
   INSTALLDESKTOPENTRY: ${INSTALLDESKTOPENTRY}
   VALIDATEDOWNLOADS: ${VALIDATEDOWNLOADS}
   CHECKDEPENDENCIES: ${CHECKDEPENDENCIES}
   PRINTHELP: ${PRINTHELP}
EOF

	if echo "${PREFIX}" | grep -q '~' ; then
		echo ""
		warn "PREFIX contains unsupported '~' character. Use \$HOME instead of ~.\n"
	fi

	if [ "${CHECKDEPENDENCIES}" = "yes" ]; then
		task "Checking dependencies"
		check_dependencies
	else
		info "Skipping dependencies check"
	fi

	task "Waiting 10 seconds before installing..."
	cat - <<EOF
   Hit Ctrl+c to cancel if you don't like the settings.
   You can also execute the following command if you want to see the
   help message with examples for this script:
      curl ${EASYINSTALLURL} | PRINTHELP=yes bash
EOF
	printf '10...'
	sleep 1
	printf '9...'
	sleep 1
	printf '8...'
	sleep 1
	printf '7...'
	sleep 1
	printf '6...'
	sleep 1
	printf '5...'
	sleep 1
	printf '4...'
	sleep 1
	printf '3...'
	sleep 1
	printf '2...'
	sleep 1
	printf '1...'
	sleep 1
	printf '0\n'

	# Create and goto a temporary directory for downloading the files
	TEMPINSTALLDIR="$(mktemp -d -q)"
	if [ -z "${TEMPINSTALLDIR}" ] || [ ! -d "${TEMPINSTALLDIR}" ]; then
		die "ERROR: Failed to created a temporary directory. Please check if mktemp works."
	fi
	pushd "${TEMPINSTALLDIR}" >/dev/null


	task "Downloading files"
	download_file "quartz"
	download_file "quartz.sha256sums"

	## system files
	#if [ "${INSTALLSYSTEMFILES}" = "yes" ] || [ "${INSTALLSYSTEMFILES}" = "yes-dontsetcurrent" ]; then
	#	#download_file "quartz-system.qcow2"
	#	download_file "quartz-system.qcow2.gz" # Will automatically uncompress
	#	download_file "quartz-system.buildinfo"
	#	#download_file "quartz-system.buildlog"

	#	# OpenSBI
	#	download_file "opensbi_lp64_generic_firmware_fw_jump.elf.COPYING.BSD"
	#	download_file "opensbi_lp64_generic_firmware_fw_jump.elf.ThirdPartyNotices.md"
	#	download_file "opensbi_lp64_generic_firmware_fw_jump.elf.URL"
	#	download_file "opensbi_lp64_generic_firmware_fw_jump.elf"

	#	# U-Boot
	#	download_file "u-boot-qemu-riscv64-smode-uboot.elf"
	#	download_file "u-boot-qemu.copyright"
	#fi

	# desktop entry
	if [ "${INSTALLDESKTOPENTRY}" = "yes" ]; then
		download_file "Quartz.desktop"
		download_file "logo.svg"
		download_file "logo-48x48.png"
		download_file "logo-64x64.png"
		download_file "logo-192x192.png"
	fi


	if [ "${VALIDATEDOWNLOADS}" = "yes" ]; then
		task "Validating downloaded files"
		sha256sum --check --ignore-missing --strict "./quartz.sha256sums"
	else
		info "Skipping validating downloaded files"
	fi

	task "Installing files"
	info "Installing quartz executable"
	install -v -m 755 -D -t "${PREFIX}/bin" "quartz"

	#if [ "${INSTALLSYSTEMFILES}" = "yes" ] || [ "${INSTALLSYSTEMFILES}" = "yes-dontsetcurrent" ]; then
	#	info "Installing quartz system files"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "quartz.sha256sums"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "quartz-system.qcow2"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "quartz-system.buildinfo"
	#	#install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "quartz-system.buildlog"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "opensbi_lp64_generic_firmware_fw_jump.elf.COPYING.BSD"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "opensbi_lp64_generic_firmware_fw_jump.elf.ThirdPartyNotices.md"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "opensbi_lp64_generic_firmware_fw_jump.elf.URL"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "opensbi_lp64_generic_firmware_fw_jump.elf"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "u-boot-qemu-riscv64-smode-uboot.elf"
	#	install -v -m 644 -D -t "${PREFIX}/share/quartz/v${RELEASEVERSION}/" "u-boot-qemu.copyright"
	#	if [ "${INSTALLSYSTEMFILES}" = "yes" ]; then
	#		ln -s -f -v -T "v${RELEASEVERSION}" "${PREFIX}/share/quartz/current"
	#	fi
	#fi

	if [ "${INSTALLDESKTOPENTRY}" = "yes" ]; then
		info "Installing quartz desktop entry"
		install -v -m 644 -D -T "logo-48x48.png" "${PREFIX}/share/icons/hicolor/48x48/apps/quartz.png"
		install -v -m 644 -D -T "logo-64x64.png" "${PREFIX}/share/icons/hicolor/64x64/apps/quartz.png"
		install -v -m 644 -D -T "logo-192x192.png" "${PREFIX}/share/icons/hicolor/192x192/apps/quartz.png"
		install -v -m 644 -D -T "logo.svg"       "${PREFIX}/share/icons/hicolor/scalable/apps/quartz.svg"
		install -v -m 644 -D -t "${PREFIX}/share/applications/" "Quartz.desktop"
	fi

	popd >/dev/null
	rm -rf "${TEMPINSTALLDIR}"


	echo ""
	info "=> Installation of Quartz ${RELEASEVERSION} was successful! <="
	echo ""
	info "If you want to uninstall quartz, just delete the installed files"
	info "from the previous step."
	echo ""

}

main

ISSUCCESS=true
exit 0
