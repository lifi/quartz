#!/bin/bash

set -e
set -o pipefail
#set -x

PACKAGES="
# The following is a list of packages to be installed in Quartz.
# One package per line.
# The packages are installed via apt-get inside of mmdebstrap.
# Lines starting with # are ignored.

# Essential packages can be printed with: aptitude search '~E'
## Essentials (installed by default):
# base-files
# base-passwd
# bash
# bsdutils
# coreutils
# dash
# debianutils
# diffutils
# dpkg
# findutils
# grep
# gzip
# hostname
# init-system-helpers
# libc-bin
# ncurses-base
# ncurses-bin
# perl-base
# sed
# sysvinit-utils
# tar
# util-linux

# Linux and stuff to get it running:
# linux-image-riscv64 # using custom kernel instead
# linux-libc-dev # using custom kernel instead
# u-boot-menu # using custom extlinux.conf
runit
runit-init
login

## Networking, SSH and Plan9
ifupdown
isc-dhcp-client
#dhcpcd
#udhcpc
resolvconf
tinysshd
ucspi-tcp
inotify-tools

## Time
#openntpd
#ntpdate

## Useful:
#e2fsprogs
lsb-base
mawk
procps
#apt-utils
#apt
debian-archive-keyring
less
readline-common
passwd
man-db
#tmux
#busybox
#neofetch # Using quartzfetch instead
file
xxd
strace
ltrace

## Editors
nano
vim-common
vim-tiny
emacs-nox


## Technische Informatik / GNU toolchain
build-essential
binutils
binutils-riscv64-linux-gnu
binutils-common
gdb
libc-bin
libc-dev-bin
make
bison
iverilog

## Documentations
glibc-doc
linux-doc
binutils-doc
manpages
manpages-dev
#manpages-posix
#manpages-posix-dev
#gdb-doc
#gcc-doc
#make-doc
#bison-doc

## Debugging Quartz
#aptitude
#apt-rdepends
#apt
#lshw
#htop
#tmux
#pciutils
"

function download_debian_keyring() {
	# Download the debian-archive keyring
	rm -rf ./keyrings
	mkdir -p ./keyrings
	pushd keyrings
	apt-get download debian-archive-keyring
	dpkg -X debian-archive-keyring_*.deb .
	find . -type f -name '*.gpg' -print0 | xargs -0 mv -t .
	popd
}


function create_debian_riscv64_chroot() {
	# Create a debian riscv64 chroot
	# https://wiki.debian.org/RISC-V#Creating_a_riscv64_chroot

	PACKAGES="$(echo "${PACKAGES}" | grep -v "^#" | cut -d ' ' -f 1 | tr '\n' ' ')"
	echo "Selected $(echo "${PACKAGES}" | tr ' ' '\n' | grep -v '^$' | wc -l ) packages: ${PACKAGES}"

	sudo rm ./riscv64-chroot -rf
	#sudo mmdebstrap --architectures=riscv64 --keyring="./keyrings" --mode="root" --format="directory" sid ./riscv64-chroot "deb http://deb.debian.org/debian sid main"
	#sudo mmdebstrap --architectures=riscv64 --keyring="./keyrings" --mode="root" --format="directory" --variant="custom" --include="${PACKAGES}" sid ./riscv64-chroot "deb http://deb.debian.org/debian sid main"
	sudo mmdebstrap \
		--verbose \
		--architectures=riscv64 \
		--keyring="./keyrings" \
		--mode="root" \
		--format="directory" \
		--variant="essential" --include="${PACKAGES}" \
		--customize-hook='cp -rf "./cptooptquartz" "$1/opt/quartz"' --customize-hook='chroot "$1" bash /opt/quartz/customize.sh' \
		sid ./riscv64-chroot "deb http://deb.debian.org/debian sid main"

	## Prepare for overlayfs. See also fstab
	sudo mv "./riscv64-chroot/var" "./riscv64-chroot/rovar"
	sudo mv "./riscv64-chroot/home" "./riscv64-chroot/rohome"
	sudo mkdir -p "./riscv64-chroot/var"
	sudo mkdir -p "./riscv64-chroot/home"

	echo ""
	echo "./riscv64-chroot size:"
	sudo du -hca ./riscv64-chroot | tail -n 1
}

function install_kernel() {
	sudo ./kernel.sh install "$(realpath ./riscv64-chroot/boot)"
}

function create_qcow2_file() {
	export LIBGUESTFS_DEBUG=1 LIBGUESTFS_TRACE=1

	# We need to convert the directory into a tar archive before giving it to virt-make-fs because otherwise virt-make-fs will have problems with absolute symlinks inside of the directory
	echo "Creating riscv64-chroot.tar"
	sudo rm -f ./riscv64-chroot.tar
	sudo tar -cf ./riscv64-chroot.tar -C ./riscv64-chroot .
	echo "Created riscv64-chroot.tar"

	echo "Creating quartz-system.qcow2"
	sudo virt-make-fs --partition=gpt --type=ext2 --format=qcow2 ./riscv64-chroot.tar quartz-system.qcow2
	sudo chown "${USER}:${USER}" quartz-system.qcow2
	chmod -w quartz-system.qcow2
	gzip -f -v --best -k quartz-system.qcow2
	gzip --test quartz-system.qcow2.gz
	ls -lah quartz-system.*
	echo "Created quartz-system.qcow2"
}

function print_buildinfo() {
	printf 'QUARTZ_BUILD_DATE_ISO8601="%s"\n' "$(date --iso-8601=minutes)"
	printf 'QUARTZ_BUILD_DATE="%s"\n' "$(date +%Y%m%d-%H%M)"
	printf 'QUARTZ_BUILD_COMMIT_REF="%s"\n' "$(git rev-parse --abbrev-ref HEAD)"
	printf 'QUARTZ_BUILD_COMMIT="%s"\n' "$(git rev-parse HEAD)"
	printf 'QUARTZ_BUILD_COMMIT_SHORT="%s"\n' "$(git rev-parse --short HEAD)"
}

##################################### MAIN #####################################


# Let me enter the sudo password faster.
sudo true

pushd "$(dirname "$0")"


(
	date
	time download_debian_keyring
	time create_debian_riscv64_chroot
	time install_kernel
	print_buildinfo | tee ./quartz-system.buildinfo | sudo tee ./riscv64-chroot/opt/quartz/buildinfo
	date
) 2>&1 | tee ./quartz-system.buildlog | sed 's,\n,\r\n,g'
sudo cp -vf ./quartz-system.buildlog ./riscv64-chroot/opt/quartz/quartz-system.buildlog

create_qcow2_file

popd
