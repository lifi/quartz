---
title: "Quartz verwenden"
lang: de
---

# Quartz verwenden

!!! info "tl;dr"
    Einfach Quartz starten. Es macht alles automatisch.
    Nach ein paar Minuten ist man in einer Bash Shell auf einem RISC-V Linux.

!!! tip
    Quartz kann entweder über den `quartz`-Befehl oder das Quartz-Desktopicon gestartet werden.

## Erster Start

Beim ersten Start muss Quartz noch einige Dinge vorbereiten:

1. Den Ordner zum Speichern von Dateien erstellen.
2. [Konfiguration](configure.md) erstellen.
3. SSH-Keypair erstellen.
4. Quartz System Dateien herunterladen (ca. 800MB).

Diese Schritte passieren vollständig automatisch und sind auch nur beim ersten Start notwendig.

![Erster Start](./img/erster_start.png)

Danach wird Quartz sich wie beim normalen Start verhalten.

## Normaler Start

Falls Quartz noch nicht gebootet ist, muss man zu Beginn kurz 1-2 Minuten warten, bis Quartz bereit ist.

Danach wird man automatisch in eine normale Bash Shell in Quartz verbunden.

![Normaler Start](./img/normaler_start.png)


## Programme in Quartz schreiben

In diesem Beispiel schreiben wir ein RISC-V Assembler Programm, welches direkt das Programm mit dem Exit Code 7 beendet (siehe Kapitel 2.2 "Plattform" im Skript).


!!! tip
    Quartz liefert einige gängige Texteditoren, wie z.B. nano, vim und emacs, mit.
Zunächst erstellen wir eine Datei `seven.s` mit dem folgenden Inhalt:
```assembly
# computer engineering - 2020ss
# seven
#
# a0-a2 - system call parameters
# a7    - system call id

.globl _start

.text
_start:
    addi a0, x0, 7  # exit code 7
    addi a7, x0, 93 # 93 is syscall __NR_exit
    ecall           # system call
```

Danach können wir das Programm übersetzen, binden und ausführen.
```bash
as -o seven.o seven.s
ld -o seven seven.o
./seven
echo $?
```

![](./img/seven.png)
![Demo](./img/quartzdemo.gif)


!!! info
    Eine vollständige Erklärung jeder einzelner dieser Schritte mit zusätzlichen Informationen befindet sich im Technische Informatik Skript Kapitel 2.2 "Plattform".

## Quartz beenden

Einfach `logout` oder `exit` eingeben, um Quartz zu beenden.

Sobald keine Verbindung zu Quartz mehr besteht, fährt sich die VM nach ca. 20 Sekunden von selbst runter.

## Hilfe

```bash
$ quartz help
usage: quartz [<subcommand>] [<args>]

subcommands:
    start                Initialize everything, start the VM if required and
                         connect to it. This is the default behaviour if no
                         subcommand is provided.

    help                 Print this help message.

    reset                Delete the .quartz directory. This will also delete
                         all files you created inside of Quartz and your
                         configuration.

    print_current_config Print the full configuration as it is
                         currently evaluated.

    env                  This is an advanced subcommand. It will fully load the
                         configuration, but not do anything. This subcommand
                         is useful for scripting as you can source quartz using
                         it: source quartz env
                         Quartz exports not only its configuration values but
                         also all Bash functions.

You can find further information in the documentation:
    https://lifi.pages.gwdg.de/quartz/

[18:42:21] INFO: .quartz directory: /home/jake/.quartz
[18:42:21] INFO: Your files are stored in /home/jake/.quartz/home/quartz/ .
[18:42:21] INFO: The configuration is located at /home/jake/.quartz/config .
```
