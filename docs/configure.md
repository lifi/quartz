---
title: "Konfiguration"
lang: de
---

# Konfiguration

Beim [ersten Start](usage.md#erster-start) erstellt Quartz automatisch eine Konfigurationsdatei namens `config` im [`.quartz` Ordner](faq.md#wo-ist-der-quartz-ordner).

Die Konfiguration erfolgt über Umgebungsvariablen, die mit dem Präfix `QUARTZ_` beginnen.

Die genauen Umgebungsvariablen, die möglichen Werte und die Standardwerte werden in den Kommentaren der Konfigurationsdatei erklärt.

## Beispiel

Im Folgenden ist eine unveränderte Konfigurationsdatei wie sie Quartz v1.3.0
beim ersten Start generiert.
```bash
#!/bin/sh
###############################################################################
# This is /home/jake/.quartz/config and is read by the quartz command.
# You can edit this file.
#
# Lines beginning with '#' are ignored.
#
# If this file is missing it will automatically be regenerated on the next
# execution of the quartz command.
#
# Quartz uses environment variables for configuration, thus you do not need to
# set these variables in this file here, but it is recommended to do so.
# You can alternatively set these variables in any file that environment
# variables are read from (e.g. /etc/profile, ~/.profile, ~/.bash_profile,
# ~/.bashrc) or temporarily via the export command from your shell.
# Though variables set in this file here will override variables read from the
# external environment.
#
# The only variable you CANNOT set in this file here is QUARTZ_DIR as it
# is used when determining the location of this file.
# QUARTZ_DIR specifies the working directory of the quartz command.
# This directory is used to store and read
# - the files you create and use inside of the Quartz VM,
# - this configuration file,
# - various logs, states and internal files.
# By default it will be set to either $XDG_DATA_HOME/.quartz or ~/.quartz.
# The current value of QUARTZ_DIR is "/home/jake/.quartz".
###############################################################################

# The release version of the quartz system files that quartz should use or
# download if missing.
# See https://lifi.pages.gwdg.de/quartz/ .
# The default is determined by the value stored in the installed quartz
# executable, or "unknown" if not known.
# Default: "1.3.0"
#QUARTZ_RELEASEVERSION="1.3.0"

# The amount of RAM the VM should use.
# See also the -m option in man:qemu-system-riscv64(1).
# Default: "1.9G"
#QUARTZ_MEMORY="1.9G"

# The number of lines of the VMs TTY that should be shown
# while waiting for the Quartz VM to boot.
# Set to 0 to disable showing any lines at all.
# Default: "5"
#QUARTZ_BOOT_LINES="5"
#QUARTZ_BOOT_LINES="0"

# The port quartz should listen to on the host for its internal ssh
# communication between host (your pc) and guest (the Quartz VM).
# Default: 22222
#QUARTZ_SSH_PORT=22222

# The number of seconds quartz should wait between checking if an active ssh
# connection to the VM exists. If before and after the waittime no ssh
# connection is found, the VM will automatically shutdown.
#
# Set to 0 to disable automatically shutting down the VM.
# You can also temporairly disable autoshutdown inside of the VM by deleting
# /tmp/quartz-autoshutdown-enabled.
#
# Default: 20
#QUARTZ_AUTOSHUTDOWN=20

# The name of the screen session where the VM should run in.
# You can also access the session directly and thus the TTY of the VM via:
# screen -r "quartz"
# See also man:screen(1).
# Default: "quartz"
#QUARTZ_SCREEN_SESSION="quartz"



################################## ADVANCED ###################################
# The following settings are advanced and shouldn't be relevant for most users.



# [ADVANCED]
# Whether debug output should be enabled.
# Anything other than "true" will disable this option.
# Note: Some debug messages may be printed BEFORE the config is loaded, so
# you may want to set the environment variable before you start quartz.
# Default: "false"
#QUARTZ_DEBUG="false"
#QUARTZ_DEBUG="true"

# [ADVANCED]
# The name of the ssh command that should be used to connect to the VM.
# Default: "ssh"
#QUARTZ_SSH_CMD="ssh"

# [ADVANCED]
# The name of the command that should be used to start the VM.
# Default: "qemu-system-riscv64"
#QUARTZ_QEMU_CMD="qemu-system-riscv64"

# [ADVANCED]
# The name of the screen command.
# Default: "screen"
#QUARTZ_SCREEN_CMD="screen"

# [ADVANCED]
# The path of the log file of the VMs TTY on the host system.
# The path can either be absolute or relative to the quartz working directory.
# Default: "serial.log"
#QUARTZ_LOG_FILE="serial.log"

# [ADVANCED]
# The timeout in seconds quartz should use for the internal ssh connection.
# See also ConnectTimeout in man:ssh_config(5).
# Default: 10000
#QUARTZ_SSH_TIMEOUT=10000

# [ADVANCED]
# Extra arguments that should be added to the qemu command.
# Default: ""
#QUARTZ_QEMU_EXTRA_ARGS=""

# [ADVANCED]
# Extra arguments that should be added to the ssh command.
# Quartz will automatically append " -t -t " to this variable, if it is used
# for the interactive shell.
# Default: ""
#QUARTZ_SSH_EXTRA_ARGS=""

# [ADVANCED]
# The command that should be used for starting the interactive shell inside
# the VM.
# Default: "/bin/bash -l"
#QUARTZ_SHELL_CMD="/bin/bash -l"

# [ADVANCED]
# The directory where quartz should store and look for the quartz system files.
# The path should either be relative to QUARTZ_DIR or absolute. The path should
# not contain spaces or other special characters.
# Note that quartz may throw an error if the current user does not have write
# permissions on the directory and the directory does not contain the files
# of the required release version.
# Default: "./system"
#QUARTZ_SYSTEM_DIR="./system"

```

