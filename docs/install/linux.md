---
title: "Quartz installieren"
lang: de
---

# Quartz auf Linux installieren

!!! tip
    Auf **Windows** und **MacOS** kann Quartz über [Lifi](https://lifi.pages.gwdg.de/lifi-dokumentation/) verwendet werden, wo Quartz drin installiert ist.

## Abhängigkeiten installieren

Zunächst müssen die folgenden Abhängigkeiten installiert werden.

=== "Debian/Ubuntu"

    ``` bash
    sudo apt install qemu-system-misc screen openssh-client curl bash grep sed gzip coreutils
    ```

=== "Arch/Manjaro"

    ``` bash
    sudo pacman -S --needed qemu-system-riscv screen openssh curl bash grep sed gzip coreutils
    ```

=== "Andere"

    - [QEMU RISC-V 64bit System emulator (`qemu-system-riscv64`)](https://www.qemu.org/docs/master/system/target-riscv.html)
    - [OpenSSH](https://www.openssh.com/)
    - [curl](https://curl.se/)
    - [GNU Screen](https://www.gnu.org/software/screen/)
    - [GNU Bash](https://www.gnu.org/software/bash/)
    - [GNU Grep](https://www.gnu.org/software/grep/)
    - [GNU sed](https://www.gnu.org/software/sed/)
    - [GNU Gzip](https://www.gnu.org/software/gzip/)
    - [GNU core utilities](https://www.gnu.org/software/coreutils/)

## Quartz installieren

[![Aktuelle Version](RELEASESVGBADGE?key_text=Aktuelle+Version)](https://lifi.pages.gwdg.de/quartz/install/linux/)

Quartz verwendet ein Skript, um die Installation zu vereinfachen.
Dieses Skript wird über Umgebungsvariablen konfiguriert.
Suche dir **einen** der folgenden Befehle aus um Quartz zu installieren:


- Installier v1.3.1 lokal im Nutzerverzeichnis:
```bash
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | RELEASEVERSION="1.3.1" PREFIX="$HOME/.local" bash
```
    (Diese Variante geht davon aus, dass `$HOME/.local/bin` in `$PATH` enthalten ist.)


- Installier v1.3.1 systemweit:
```bash
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | sudo RELEASEVERSION="1.3.1" PREFIX="/usr/local" bash
```

- Gebe die Hilfe vom `easyinstall.sh`-Skript und die Erklärung der Umgebungsvariablen aus, statt Quartz zu installieren:
```bash
curl https://lifi.pages.gwdg.de/quartz/easyinstall.sh | PRINTHELP=yes bash
```


!!! info
    Bei der Installation werden einige Dateien im `PREFIX` Ordner gespeichert.
    Um Quartz zu deinstallieren, einfach diese Dateien löschen.

![Installation von Version 1.0.0 im Nutzerverzeichnis](../img/installation.png)

### Versionen

#### 1.3.1

- Changelog:
    - "Ungültiger Alias mv -vr" gefixt. (lifi/quartz#17)
- `RELEASEVERSION=1.3.1`

#### 1.3.0

- Changelog:
    - "Cannot boot quartz if CDROM drive installed on host" gefixt. (lifi/quartz#12)
    - Einstellungsoption `QUARTZ_SYSTEM_DIR` hinzugefügt. (lifi/quartz#13)
- `RELEASEVERSION=1.3.0`

#### 1.2.0

- Changelog:
    - "Assembling code with Jump instructions results in Segmentation Faults" gefixt (durch `alias as='as -fPIC'`). (lifi/quartz#7)
    - "SSH Connection closes after ca. 1 hour of inactivity" gefixt. (lifi/quartz#10)
    - Dokumentationspakete installiert. (lifi/quartz#11)
        - `manpages`
        - `manpages-dev`
        - `binutils-doc`
        - `glibc-doc`
        - `linux-doc`
    - `ltrace` und `strace` installiert. (b6cda9a12bbdb14034a8a78658a271c3129d8276)
    - Bootdetection verändert. (lifi/quartz#9)
        - Automatisches `screen -wipe` auf dem Hostsystem. (20020793a1ceb07c317844e55290679335b0f7d4)
    - Kleine Veränderungen an der Kernelkonfiguration. (e7f8bd4ddb0b1d0a64a446eb5410e91bcd603f2e)
    - Softwarepakete aktualisiert.
- `RELEASEVERSION=1.2.0`

#### 1.1.0

- Changelog:
    - `emacs` installiert.
    - Subcommands hinzugefügt. Siehe `quartz help`.
    - Dokumentation verlinkt.
    - Quartz für Nutzer mit UID ungleich 1000 gefixt.
- `RELEASEVERSION=1.1.0`

#### 1.0.0

- Changelog:
    - Erster Quartz Release.
- `RELEASEVERSION=1.0.0`


## Fertig

Hurra! Auf der nächsten Seite erfährst du wie du Quartz startest und verwendest.











