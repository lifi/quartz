---
title: "Startseite"
lang: de
---


# Quartz

[![Aktuelle Version](RELEASESVGBADGE?key_text=Aktuelle+Version)](https://lifi.pages.gwdg.de/quartz/install/linux/)

Quartz ist eine QEMU RISC-V 64bit Virtuelle Maschine (VM) mit einem Mini-Linux für Technische Informatik.

---

<div class="text-center" >
<a href="install/linux/"            class="btn btn-primary attention-btn"    style="--animation-delay: 0.2s;" role="button">Quartz installieren</a>
<a href="usage/"                    class="btn btn-secondary attention-btn"  style="--animation-delay: 0.4s;" role="button">Quartz verwenden</a>
</div>
<br>

---

<div class="text-center" >
<img src="./img/quartzdemo.gif" alt="Quartz Demo" />
</div>

---

<div class="text-center" >
<a href="configure/" class="btn btn-secondary"  style="--animation-delay: 0.2s;" role="button">Konfiguration</a>
</div>

---

<div class="text-center" >
<a href="faq/" class="btn btn-tertiary"  style="--animation-delay: 0.2s;" role="button">FAQ</a>
<a href="https://gitlab.gwdg.de/lifi/quartz/-/issues" class="btn btn-tertiary"  style="--animation-delay: 0.2s;" role="button">Bug & Issue Tracker</a>
</div>

---

Quartz ist Teil vom [Lifi Projekt](https://lifi.pages.gwdg.de/lifi-dokumentation/).
