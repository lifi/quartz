---
lang: de
---

# FAQ

---

## Wo werden die Dateien, die ich in Quartz erstelle, gespeichert?

Im `.quartz` Ordner ist ein `home/quartz/` Ordner, in dem deine Dateien gespeichert werden.

Der vollständige Pfad wird auch angezeigt, wenn man `quartz help` ausführt.

Man kann dort auch Dateien drin ablegen, um sie in Quartz zu nutzen.

---

## Wo ist der `.quartz` Ordner?

Der `.quartz` Ordner befindet sich entweder direkt im Homeverzeichnis des Nutzers oder im `$XDG_DATA_HOME` Ordner, falls diese Umgebungsvariable gesetzt ist.

Alternativ, falls die Umgebungsvariable `$QUARTZ_DIR` gesetzt ist, *überschreibt* diese den Ort des `.quartz` Ordner.

Wenn man `quartz help` ausführt, wird auch der vollständige Pfad zum `.quartz` Ordner ausgegeben.

!!! info
    Der `.quartz` Ordner beinhaltet

    - die [Konfigurationsdatei](configure.md),
    - verschiedene Logdateien,
    - die rohen Quartz Systemdateien,
    - verschiedene weitere Dateien die beim und zum Ausführen von Quartz wichtig sind, und
    - das Homeverzeichnis des `quartz` Nutzers (beinhaltet die Dateien die man in Quartz erstellt).

---

## Was sind die Systemanforderungen für Quartz?

- Freier Festplattenspeicher: min. 1GB (empfohlen 5GB)
- RAM: min. 2GB
- OS: Ein modernes Linux auf dem sich QEMU installieren und ausführen lässt.

---

## Was für ein Linux ist Quartz?

Quartz ist einfach nur ein [RISC-V 64bit Debian sid](https://wiki.debian.org/RISC-V) mit ein paar Veränderungen, vorinstalliert in einer VM.

---

## Wie installiere ich neuere Versionen von Quartz?

Beim [Installieren von Quartz](install/linux.md) setzt die `RELEASEVERSION` Umgebungsvariable welche Version von Quartz installiert werden soll.

Möchte man einfach nur eine andere Version vom Quartz System verwenden kann man dies auch in der [Konfigurationsdatei](configure.md) mit der `QUARTZ_RELEASEVERSION` Variable steuern.

---

## Welche Texteditoren gibt es in Quartz?

- [Nano](https://www.nano-editor.org/)
- [Vim](https://www.vim.org/) (in der [common](https://manpages.debian.org/stretch/vim-common/vim.1.en.html) und [tiny](https://manpages.debian.org/stretch/vim-tiny/vi.1.en.html) Variante)
- [Emacs](https://www.gnu.org/software/emacs/) (ohne GUI Support) (seit Quartz v1.1.0)

!!! tip "Externe Texteditoren"
    Im `.quartz` Ordner existiert ein `home/quartz/` Ordner, welcher die Dateien die man in Quartz erstellt beinhaltet. In diesem Ordner kann man auf dem Hostrechner ohne Probleme auch externe Texteditoren wie z.B. VSCodium verwenden.

---

## Wie kann ich Quartz beenden?

Die Quartz VM fährt sich automatisch nach ca. 20 Sekunden herunter, wenn keine Verbindung mehr zu ihr besteht.
Heißt Befehle wie `exit` oder `logout` reichen komplett aus.

Explizites kann man Quartz aber auch mit `poweroff` oder `shutdown` herunterfahren.

---

## Wie installiere ich Software in Quartz?

Das Quartz System ist read-only, heißt es kann Software nicht einfach nachinstalliert werden.

Erstelle stattdessen ein Issue im [Quartz Issue Tracker](https://gitlab.gwdg.de/lifi/quartz/-/issues) wo du fragst, dass die bestimmte Software installiert werden soll.

Sobald dein Issue angenommen wird, kannst du einfach die neuere Version von Quartz installieren, wo die Software dann installiert ist.

---

## Was sind die Nutzerdaten in Quartz?

In den allermeisten Fällen sollte man diese Daten eigentlich nicht brauchen, aber für den Fall das doch:

- Nutzername: `quartz` Passwort: `quartz`
- Nutzername: `root` Passwort: `root`

---

## Wieso bekomme ich einen Segmentation Fault beim Ausführen meines Programmes, obwohl es korrekt ist?

In älteren Quartz Versionen (≤v1.1.0) existierte ein Bug wo Assembly Code welcher einen Sprungbefehl (`j`, `jal`, `jr`, etc.) enthalten hat, beim Ausführen immer einen Segmentation Fault verursacht hat.

Dieser Bug sollte ab Quartz v1.2.0 gefixt sein. Siehe ["Wie installiere ich neuere Versionen von Quartz?"](#wie-installiere-ich-neuere-versionen-von-quartz) für Informationen zum Aktualisieren von Quartz.

Für weitere Informationen zu diesem Bug siehe [Issue #7 (Assembling code with Jump instructions results in Segmentation Faults)](https://gitlab.gwdg.de/lifi/quartz/-/issues/7).

!!! tip "Workaround"
    In älteren Quartz Versionen oder in Bash-Skripten/Makefiles kann als Workaround beim Bauen die `-fPIC` Flagge an den Befehl gehängt werden: `as -fPIC -o code.o code.s`

---

