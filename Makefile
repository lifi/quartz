prefix = /usr

.PHONY: default
default: build

.PHONY: clean
clean:
	sudo $(RM) -r ./riscv64-chroot
	$(RM) -r ./keyrings

.PHONY: clean-kernel
clean-kernel:
	./kernel.sh clean

.PHONY: clean-all
clean-all: clean clean-deps clean-kernel
	$(RM) quartz-system.buildlog
	$(RM) quartz-system.qcow2
	$(RM) quartz-system.qcow2.sha256sum
	$(RM) kernel.config.old

.PHONY: clean-deps
clean-deps:
	$(RM) -r deps/opensbi
	$(RM) -r deps/opensbi-*
	$(RM) -r deps/opensbi_*
	$(RM) -r deps/u-boot-qemu
	$(RM) -r deps/u-boot-qemu*

.PHONY: menuconfig
menuconfig:
	./kernel.sh menuconfig

.PHONY: build
build: kernel deps
	./build.sh

.PHONY: deps
deps:
	./deps/download.sh

.PHONY: kernel
kernel: riscv64-linux/linux/arch/riscv/boot/Image


.PHONY: serve-docs
serve-docs:
	mkdocs serve

.PHONY: docs
docs:
	mkdocs build --strict --verbose
	cp -v easyinstall.sh public/
	find ./public/ -type f -exec sh -c 'sed -i "s,RELEASESVGBADGE,https://gitlab.gwdg.de/lifi/quartz/-/badges/release.svg,g" {}' ';'




riscv64-linux/linux/arch/riscv/boot/Image: kernel.config
	./kernel.sh build
