#!/bin/bash

set -e # Exit on error

pushd "$(dirname "$0")" >/dev/null

# Download OpenSBI if missing
# https://github.com/riscv-software-src/opensbi
export OPENSBIVERSION="1.4"
export OPENSBISRCDIR="opensbi-${OPENSBIVERSION}-rv-bin"
if [ ! -d "${OPENSBISRCDIR}" ]; then

	wget "https://raw.githubusercontent.com/riscv-software-src/opensbi/v${OPENSBIVERSION}/ThirdPartyNotices.md" -O "./opensbi_lp64_generic_firmware_fw_jump.elf.ThirdPartyNotices.md"
	wget "https://raw.githubusercontent.com/riscv-software-src/opensbi/v${OPENSBIVERSION}/COPYING.BSD" -O "./opensbi_lp64_generic_firmware_fw_jump.elf.COPYING.BSD"
	echo "https://github.com/riscv-software-src/opensbi/releases/tag/v${OPENSBIVERSION}" > "./opensbi_lp64_generic_firmware_fw_jump.elf.URL"
	wget "https://github.com/riscv-software-src/opensbi/releases/download/v${OPENSBIVERSION}/opensbi-${OPENSBIVERSION}-rv-bin.tar.xz" -O opensbi.tar.xz
	# TODO verify signature
	tar xf opensbi.tar.xz
	rm -f opensbi.tar.xz
	ln -sf "${OPENSBISRCDIR}" "./opensbi"
	ln -sf "./opensbi/share/opensbi/lp64/generic/firmware/fw_jump.elf" "./opensbi_lp64_generic_firmware_fw_jump.elf"


fi

# Download u-boot-qemu
# https://deb.debian.org/debian/pool/main/u/u-boot/
UBOOTURL="https://deb.debian.org/debian/pool/main/u/u-boot/u-boot-qemu_2024.01+dfsg-7_all.deb"
if [ ! -d "./u-boot-qemu" ]; then
	mkdir -p ./u-boot-qemu
	pushd u-boot-qemu
	#apt-get download debian-archive-keyring
	wget "${UBOOTURL}" -O u-boot-qemu.deb || ( popd ; rm -rvf u-boot-qemu ; exit 1 ; )
	dpkg -X u-boot-qemu.deb .
	rm -f u-boot-qemu.deb
	popd
	ln -sf "./u-boot-qemu/usr/lib/u-boot/qemu-riscv64_smode/uboot.elf" "./u-boot-qemu-riscv64-smode-uboot.elf"
	ln -sf "./u-boot-qemu/usr/share/doc/u-boot-qemu/copyright" "./u-boot-qemu.copyright"
fi

popd >/dev/null
