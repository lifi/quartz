#!/bin/bash
set -e
pushd "$(dirname "$0")"

#### CONFIG
export QUARTZ_RELEASEVERSION="local"
# export QUARTZ_RELEASEVERSION="1.1.0"
export QUARTZ_DEBUG="true"
export QUARTZ_BOOT_LINES=25

QUARTZ_DIR="$(pwd)/.quartz"
export QUARTZ_DIR

#### DELETE OLD QUARTZ_DIR TO ENSURE REPEATABLE TESTS
rm -rf "$QUARTZ_DIR"

#### USE LOCAL FILES
if [ "$QUARTZ_RELEASEVERSION" = "local" ]; then
	mkdir -p "${QUARTZ_DIR}/system/${QUARTZ_RELEASEVERSION}"
	pushd "${QUARTZ_DIR}/system/${QUARTZ_RELEASEVERSION}"
	# OpenSBI
	ln -s -f -t . "../../../deps/opensbi_lp64_generic_firmware_fw_jump.elf.COPYING.BSD"
	ln -s -f -t . "../../../deps/opensbi_lp64_generic_firmware_fw_jump.elf.ThirdPartyNotices.md"
	ln -s -f -t . "../../../deps/opensbi_lp64_generic_firmware_fw_jump.elf.URL"
	ln -s -f -t . "../../../deps/opensbi_lp64_generic_firmware_fw_jump.elf"

	# U-Boot
	ln -s -f -t . "../../../deps/u-boot-qemu-riscv64-smode-uboot.elf"
	ln -s -f -t . "../../../deps/u-boot-qemu.copyright"

	# Quartz System
	ln -s -f -t . "../../../quartz-system.qcow2"
	popd
fi

#### RUN QUARTZ
#./bin/quartz print_current_config
./bin/quartz

popd
