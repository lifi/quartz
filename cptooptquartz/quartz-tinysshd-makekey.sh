#!/bin/sh
### BEGIN INIT INFO
# Provides:          quartz-tinysshd-makekey
# Required-Start:
# Required-Stop:
# Should-Start:
# Default-Start:     S
# Default-Stop:
# Short-Description: Create host keys for tinysshd if missing.
# Description:       Create host keys for in /var/lib/tinyssh/sshkeydir if missing.
### END INIT INFO

PATH=/usr/sbin:/usr/bin:/sbin:/bin
. /lib/lsb/init-functions

TINYSSHKEYDIR=/var/lib/tinyssh/sshkeydir

do_start () {
	if [ -d "${TINYSSHKEYDIR}" ]; then
		log_success_msg "${TINYSSHKEYDIR} already exists. Nothing to do."
	else
		mkdir -p "${TINYSSHKEYDIR}"
		rm -rf "${TINYSSHKEYDIR}"
		tinysshd-makekey "${TINYSSHKEYDIR}"
		log_success_msg "Created keys in ${TINYSSHKEYDIR}."
	fi
	return 0
}

case "$1" in
  start|"")
	do_start
	;;
  restart|reload|force-reload)
	echo "Error: argument '$1' not supported" >&2
	exit 3
	;;
  stop|status)
	# No-op
	;;
  *)
	echo "Usage: quartz-tinysshd-makekey.sh [start|stop]" >&2
	exit 3
	;;
esac

exit 0
