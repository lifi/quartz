#!/bin/bash

# This file gets exectued inside of the chroot environment after it has been created and is used to apply the Quartz customizations

# Based on https://wiki.debian.org/RISC-V#Preparing_the_chroot_for_use_in_a_virtual_machine

set -e
set -x

# Set hostname
echo "Quartz" > /etc/hostname
#hostname "Quartz"

# Set global environment variables
cp -vf /opt/quartz/profile.sh /etc/profile.d/quartz.sh
source /etc/profile

# Install extra packages
if ls /opt/quartz/packages/*.deb ; then
	dpkg -R -i /opt/quartz/packages/
fi

# Set up basic networking
cat >>/etc/network/interfaces <<EOF
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
EOF
mv -vf /opt/quartz/networking /etc/default/networking

# Update /etc/skel
cp -rvf -t /etc/skel/ /opt/quartz/skel/.b*

# Create quartz account
useradd --create-home --user-group --shell "/bin/bash" -e "" -f -1 quartz

## Set passwords
echo -e 'root\nroot' | passwd root
echo -e 'quartz\nquartz' | passwd quartz

# Disable the getty on hvc0 as hvc0 and ttyS0 share the same console device in qemu.
ln -sf /dev/null /etc/systemd/system/serial-getty@hvc0.service  

## Configure ntp tools
#sed -i 's/^DAEMON_OPTS="/DAEMON_OPTS="-s /' /etc/default/openntpd

## Configure filesystems
cp -vf /opt/quartz/fstab /etc/fstab

## Remove checkroot-bootclean as all the directories cleared by this are tmpfs anyway
## and it throws errors during boot.
rm -vf /etc/rcS.d/S06checkroot-bootclean.sh

## Configure tinysshd services
cp -vf /opt/quartz/quartz-tinysshd-makekey.sh /etc/init.d/
ln -sf ../init.d/quartz-tinysshd-makekey.sh /etc/rcS.d/S50quartz-tinysshd-makekey.sh
mv -vf /opt/quartz/service/quartz-tinysshd /etc/sv/quartz-tinysshd
ln -sf /etc/sv/quartz-tinysshd /etc/runit/runsvdir/default/

## Configure autoshutdown service
mv -vf /opt/quartz/service/quartz-autoshutdown /etc/sv/quartz-autoshutdown
ln -sf /etc/sv/quartz-autoshutdown /etc/runit/runsvdir/default/

## Configure reown service
mv -vf /opt/quartz/service/quartz-reown /etc/sv/quartz-reown
ln -sf /etc/sv/quartz-reown /etc/runit/runsvdir/default/

## Configure debug service
mv -vf /opt/quartz/service/quartz-debug /etc/sv/quartz-debug
ln -sf /etc/sv/quartz-debug /etc/runit/runsvdir/default/

## Let normal user run chown
chmod u+s /usr/bin/chown

## Let normal user shutdown Quartz
chmod u+s /lib/runit/shutdown
ln -sf /lib/runit/shutdown /bin/shutdown
ln -sf /lib/runit/shutdown /bin/poweroff

## Configure additional shutdown procedure
mv -vf /opt/quartz/rc.shutdown /etc/runit/rc.shutdown
chmod +x /etc/runit/rc.shutdown

## Configure syslinux-style boot menu
mkdir -p /boot
mv -vf /opt/quartz/extlinux /boot/
#cat >>/etc/default/u-boot <<EOF
#U_BOOT_PARAMETERS="ro noquiet root=/dev/vda1"
#U_BOOT_FDT_DIR="noexist"
#EOF
#u-boot-update

exit 0
