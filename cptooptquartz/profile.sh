#!/bin/sh

export PATH="${PATH}:/opt/quartz/bin"

# workaround for fixing assembling code with Jump instructions resulting in Segmentation Faults
# https://gitlab.gwdg.de/lifi/quartz/-/issues/7
alias as='as -fPIC'

