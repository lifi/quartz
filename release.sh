#!/bin/bash

set -Eeuo pipefail # Exit on error

if [ "${#}" -ne 1 ] || (! echo "${1}" | grep -q -E "^[0-9]+\.[0-9]+\.[0-9]+$") ; then
	echo "Usage: $0 <RELEASEVERSION>"
	echo "    RELEASEVERSION: X.X.X"
	exit 1
fi

RELEASEVERSION="${1}"

glab auth status -h gitlab.gwdg.de

GITLABPROJECTID="$(glab api 'projects/:id' | jq -r '.id')"
if [ -z "${GITLABPROJECTID}" ]; then
	echo "Failed to fetch gitlab project id."
	exit 1
fi

function upload_file() {
	SRCFILE="${1}"
	FILENAME="$(basename "${SRCFILE}")"
	PACKAGENAME="quartz"

	DLLINK="https://gitlab.gwdg.de/api/v4/projects/${GITLABPROJECTID}/packages/generic/${PACKAGENAME}/${RELEASEVERSION}/${FILENAME}"
	echo "Uploading '${SRCFILE}' as ${DLLINK}"

	#glab alias set upload-package 'api projects/:id/packages/generic/$1 -X PUT --input $2'
	#glab upload-package "quartz/${RELEASEVERSION}/${FILENAME}" "${SRCFILE}"
	glab api "projects/${GITLABPROJECTID}/packages/generic/${PACKAGENAME}/${RELEASEVERSION}/${FILENAME}" -X "PUT" --input "${SRCFILE}"
}

function upload_dir() {
	find -L "${1}" -type f | while read -r line; do
		upload_file "${line}"
	done
}

rm -rf preparerelease
mkdir -p preparerelease
pushd "preparerelease" >/dev/null

cp -t . "../bin/quartz" # cp instead of ln because the RELEASEVERSION needs to be stored inside
ln -s -f -t . "../quartz-system.qcow2"
ln -s -f -t . "../quartz-system.qcow2.gz"
ln -s -f -t . "../quartz-system.buildinfo"
ln -s -f -t . "../quartz-system.buildlog"

# desktop entry
ln -s -f -t . "../Quartz.desktop"
ln -s -f -t . "../docs/img/logo/logo.svg"
ln -s -f -t . "../docs/img/logo/logo-48x48.png"
ln -s -f -t . "../docs/img/logo/logo-64x64.png"
ln -s -f -t . "../docs/img/logo/logo-192x192.png"

# OpenSBI
ln -s -f -t . "../deps/opensbi_lp64_generic_firmware_fw_jump.elf.COPYING.BSD"
ln -s -f -t . "../deps/opensbi_lp64_generic_firmware_fw_jump.elf.ThirdPartyNotices.md"
ln -s -f -t . "../deps/opensbi_lp64_generic_firmware_fw_jump.elf.URL"
ln -s -f -t . "../deps/opensbi_lp64_generic_firmware_fw_jump.elf"

# U-Boot
ln -s -f -t . "../deps/u-boot-qemu-riscv64-smode-uboot.elf"
ln -s -f -t . "../deps/u-boot-qemu.copyright"

# Store RELEASEVERSION in quartz
sed -i "/QUARTZ_RELEASEVERSION_SED_MARKER/s,=\"[^\"]*\",=\"${RELEASEVERSION}\",g" "./quartz"

# Create Checksums
sha256sum ./* | tee quartz.sha256sums

# Don't upload the uncompressed version, but still have it sha256sum calculated
rm -f "./quartz-system.qcow2"

ls -lah ./*

popd >/dev/null

upload_dir "preparerelease"
